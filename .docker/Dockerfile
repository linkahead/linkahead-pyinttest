FROM debian:bullseye
RUN apt-get update && \
    apt-get install \
    curl \
    git \
    openjdk-11-jdk-headless \
    python3-autopep8 \
    python3-pip \
    tox \
    -y
ARG PYLIB
RUN echo "PYLIB=${PYLIB}"
COPY .docker/wait-for-it.sh /wait-for-it.sh
COPY . /git
RUN pip install -r /git/requirements.txt
ADD https://gitlab.indiscale.com/api/v4/projects/97/repository/commits/${PYLIB} \
    pylib_version.json
RUN git clone https://gitlab.indiscale.com/caosdb/src/caosdb-pylib.git && \
    cd caosdb-pylib && git checkout ${PYLIB} && pip3 install .

# Delete .git because it is huge.
RUN rm -r /git/.git

# Install pylinkahead.ini for the tests
RUN mv /git/.docker/tester_pylinkahead.ini /git/pylinkahead.ini


WORKDIR /git
# wait for server,
CMD /wait-for-it.sh caosdb-server:10443 -t 500 -- \
    # ... install pylinkahead.ini and the server-side scripts
    cp /git/.docker/sss_pylinkahead.ini /scripting/home/.pylinkahead.ini && \
    cp -r /git/resources /scripting/bin-debug && \
    # ... put out general version information
    python3 --version && \
    python3 -c "import linkahead; print(linkahead.version.version)" && \
    # ... and run tests
    pytest --cov=linkahead -vv tests
