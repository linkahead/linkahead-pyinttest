# README

## Welcome

This is the **Linkahead Python Integration Testing** repository and a part of the
Linkahead project.

## Getting started #

### Setup

Dependencies can be installed using  `pip install -r requirements.txt`.  

The tests must be run against a correctly configured linkahead server. The suggested
way to achieve this is to start linkahead using the [test_profile](/test_profile/profile.yml)
provided with this repository. Instructions can be found in the official LinkAhead
[documentation](https://docs.indiscale.com//caosdb-deploy/README_TEST.html#integration-tests).

After a linkahead server is set up, copy the content of 
[`pylinkahead.ini.template`](pylinkahead.ini.template) to a new file named `pylinkahead.ini`.

### Run the tests

The tests can then be run from the base directory using `pytest` or `pytest-3` (depending on
your system). You can also select a single test file with `pytest-3 tests/test_issues.py`,
or a test matching a regular expression, f.e. using `pytest-3 tests/test_issues.py -k issue_123`.

## Configuration for LinkAhead servers built from source

The following steps describe how to set up the tests for running against
a plain LinkAhead server built from the sources. 

- To run tests, start up a Linkahead server with the following properties
  (documentation for this can be found in the [official documentation](https://docs.indiscale.com/)):
  - The Linkahead server must have debugging enabled.
  - The database should be empty.
  - The `resources` directory should be made visible, e.g., by
    symlinking or copying its contents to the server in
    `scripting/bin-debug`.
- Modify `pylinkahead.ini.template` and save it as `pylinkahead.ini`,
  taking care of the following points:
  - Certificates must be valid and be specified in `pylinkahead.ini`.
  - Server-side scripting paths must be given, otherwise server-side
	scripting will be omitted. The remote path
	`test_server_side_scripting.bin_dir.server` should probably be
	something like
	`/opt/caosdb/git/caosdb-server/scripting/bin-debug`.
  - Paths for the file tests must exist, or be creatable by the
    testing script and the server.

## Run tests against a remote linkahead server

* There is a special mark `local_server`, which marks all test which cannot
  pass when the server is not on the host where the test suite is running.
* Start tests with `pytest -m "not local_server"` or
  `tox -- -m "not local_server"`.
* The remote linkahead server must have a TLS certificate which matches the name given
  in `pylinkahead.ini`.  If you use LinkAhead Control, this can be achieved by:
	* `linkahead cert_gen -- --hostname <your hostname>`
	* `linkahead certs -d <your profile path>/custom/other/cert`
	* `linkahead start`

## Further Reading

Please refer to the [official documentation](https://docs.indiscale.com/) for more information.

## Contributing

Thank you very much to all contributers—[past, present](https://gitlab.com/linkahead/linkahead/-/blob/dev/HUMANS.md), and prospective ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of Conduct](https://gitlab.com/linkahead/linkahead/-/blob/dev/CODE_OF_CONDUCT.md).

### How to Contribute

* You found a bug, have a question, or want to request a feature? Please
[create an issue](https://gitlab.com/linkahead/linkahead-pyinttest/-/issues).
* You want to contribute code? Please fork the repository and create a merge
request in GitLab and choose this repository as target. Make sure to select
"Allow commits from members who can merge the target branch" under Contribution
when creating the merge request. This allows our team to work with you on your request.
- You can also contact us at **info (AT) indiscale.com**.

## License

* Copyright (C) 2018 Research Group Biomedical Physics, Max Planck Institute
  for Dynamics and Self-Organization Göttingen.
* Copyright (C) 2020-2024 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under the [GNU Affero General Public
License](LICENCE.md) (version 3 or later).
