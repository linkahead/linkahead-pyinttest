# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
from caosdb import Container, Property, RecordType, Record, execute_query
import caosdb
import caosdb as db
from caosdb.connection.connection import get_connection

from caosdb.exceptions import TransactionError
from pytest import mark, raises


def setup_function(function):
    teardown_function(function)


def teardown_function(function):
    try:
        db.execute_query("FIND ENTITY").delete()
    except BaseException:
        pass


def test_default_datatype_for_recordtypes():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").add_property(name="TestRT1").insert()
    p = db.execute_query("FIND ENTITY TestRT2", unique=True).get_properties()[0]
    assert p.id == rt1.id
    assert p.name == rt1.name
    assert p.datatype == rt1.name

    p = rt2.get_properties()[0]
    assert p.id == rt1.id
    assert p.name == rt1.name
    assert p.datatype == rt1.name


def test_datatype_inheritance():

    insert = '<Insert><Property id="-1" name="test_property" description="bla" datatype="Text"/><RecordType name="test_rt" description="bla">    <Property id="-1" importance="obligatory" /></RecordType></Insert>'

    con = get_connection()
    http_response = con.insert(
        entity_uri_segment=["Entity"], body=insert)

    c = Container._response_to_entities(http_response)
    for e in c:
        assert e.is_valid()
        assert e.id is not None
        assert int(e.id) >= 100


def test_datatype_overriding():
    p = Property(
        name="DatatypeOverridingDoubleProperty",
        description="DoubleDesc",
        datatype="DOUBLE").insert()
    assert p.is_valid()

    rt = RecordType(
        name="DatatypeOverridingRT").add_property(
        p, datatype="TEXT")
    rt.insert()
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DatatypeOverridingDoubleProperty" == rt.get_properties()[0].name
    assert "DoubleDesc" == rt.get_properties()[0].description
    assert str("TEXT").lower() == rt.get_properties()[0].datatype.lower()

    # retrieve again:
    rt = execute_query("FIND ENTITY DatatypeOverridingRT", unique=True)
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DatatypeOverridingDoubleProperty" == rt.get_properties()[0].name
    assert "DoubleDesc" == rt.get_properties()[0].description
    assert str("TEXT").lower() == rt.get_properties()[0].datatype.lower()


def test_datatype_overriding_update():
    p = Property(name="DoubleProperty", datatype="DOUBLE").insert()
    assert p.is_valid()

    rt = RecordType(
        name="DatatypeOverridingRT").add_property(
        p, datatype="TEXT")
    rt.insert()
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DoubleProperty" == rt.get_properties()[0].name
    assert str("TEXT").lower() == rt.get_properties()[0].datatype.lower()

    # retrieve again:
    rt = execute_query("FIND ENTITY DatatypeOverridingRT", unique=True)
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DoubleProperty" == rt.get_properties()[0].name
    assert str("TEXT").lower() == rt.get_properties()[0].datatype.lower()

    with raises(TransactionError) as te:
        p.datatype = "INT"
        p.update()

    assert "Unknown data type." == te.value.errors[0].msg

    p.datatype = "INTEGER"
    p.update()
    assert p.is_valid()
    assert str("INTEGER").lower() == p.datatype.lower()

    # retrieve again:
    p = execute_query("FIND ENTITY DoubleProperty", unique=True)
    assert p.is_valid()
    assert str("INTEGER").lower() == p.datatype.lower()

    # retrieve rt again:
    rt = execute_query("FIND ENTITY DatatypeOverridingRT", unique=True)
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DoubleProperty" == rt.get_properties()[0].name
    # overriding still ok?
    assert str("TEXT").lower() == rt.get_properties()[0].datatype.lower()

    rt.get_properties()[0].datatype = "DATETIME"
    rt.update()
    assert rt.is_valid()
    assert rt.get_properties() is not None
    assert 1 == len(rt.get_properties())
    assert "DoubleProperty" == rt.get_properties()[0].name
    assert str("DATETIME").lower() == rt.get_properties()[0].datatype.lower()


@mark.xfail(reason="https://gitlab.com/linkahead/linkahead-server/-/issues/257")
def test_recordtype_to_record():
    rt = RecordType(name="SimpleTextRecordType")
    rt.datatype = "TEXT"
    rt.insert()
    assert rt.is_valid()
    assert rt.datatype is not None
    assert str("TEXT").lower() == rt.datatype.lower()

    rt = RecordType(id=rt.id).retrieve()
    assert rt.is_valid()
    assert rt.datatype is not None
    assert str("TEXT").lower() == rt.datatype.lower()

    rec = Record().add_parent(name="SimpleTextRecordType").insert()
    assert rec.is_valid()
    # This fails to inherit the datatype
    assert rec.datatype is not None
    assert str("TEXT").lower() == rec.datatype.lower()


def test_concrete_property():
    def test(datatype):
        try:
            p = Property(
                name="DataTypeTestDoubleProperty",
                datatype=datatype.upper()).insert()
            assert p.is_valid()
            assert p.datatype.upper() == datatype.upper()

            p2 = Property(id=p.id).retrieve()
            assert p.datatype == p2.datatype

            return p
        finally:
            try:
                p.delete()
            except BaseException:
                pass
    p_double = test("DOUBLE")
    p_datetime = test("DATETIME")
    p_integer = test("INTEGER")
    p_timespan = test("TIMESPAN")
    p_text = test("TEXT")
    p_file = test("FILE")
    p_reference = test("REFERENCE")


def test_reference_with_null_value():
    RT1 = RecordType(name="RT1").insert()
    assert RT1.is_valid()

    RT2 = RecordType(name="RT2").add_property(RT1).insert()
    assert RT2.is_valid()
    assert RT2.get_property("RT1").datatype == "RT1"

    RT2c = RecordType(name="RT2").retrieve()
    assert RT2c.is_valid()
    assert RT2c.get_property("RT1").datatype == "RT1"


def test_duplicate_properties():
    p = Property(
        name="SimpleTestProperty",
        datatype=caosdb.DOUBLE).insert()
    assert p.is_valid()

    rt = RecordType(
        name="SimpleTestRecordType").add_property(p, datatype=caosdb.TEXT).add_property(p, datatype=caosdb.INTEGER).insert()
    assert rt.is_valid()
