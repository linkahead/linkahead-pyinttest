# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
from pytest import raises, mark
import caosdb as db


def setup_function(function):
    teardown_function(function)


def teardown_function(function):
    d = db.execute_query("FIND Test*")
    if len(d) > 0:
        d.delete()


@mark.xfail(reason="Should raise an error or at least a warning. "
            "caosdb-server#171")
def test_parent_name_id_mismatch():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").add_parent(id=rt1.id, name=rt1.name +
                                                   "_wrong")
    with raises(db.TransactionError):
        rt2.insert()
        print(rt2)
