# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 16.03.2017.

@author: tf
"""
import linkahead as db
from pytest import raises


def assert_true(a):
    assert a


def assert_equal(a, b):
    assert a == b


def assert_is_not_none(a):
    assert a is not None


def setup_module():
    db.configure_connection()
    try:
        db.administration._delete_user(name="test_user")
    except Exception as e:
        print(e)
    try:
        db.execute_query("FIND ENTITY").delete()
    except Exception as e:
        print(e)
    db.RecordType(name="TestPerson").insert()
    db.Property(name="TestSignature", datatype=db.TEXT).insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    db.Record(
        name="TestJohnDoe").add_parent(
        name="TestPerson").add_property(
            name="TestSignature",
        value="tjd").insert()
    db.Property(name="TestAnimal", datatype=db.TEXT).insert()
    db.Property(name="TestRoom", datatype=db.TEXT).insert()
    db.RecordType(
        name="TestExperiment").add_property(
        name="TestAnimal",
        importance=db.OBLIGATORY).add_property(
            name="TestRoom",
        importance=db.OBLIGATORY).insert()
    db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestAnimal",
            value="Pig").add_property(
                name="TestRoom",
                value="Lab13").add_property(
                    name="TestConductor",
        value="TestJohnDoe").insert()


def teardown_module():
    try:
        db.administration._delete_user(name="test_user")
    except Exception as e:
        print(e)
    try:
        db.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)


def setup_function(function):
    pass


def teardown_function(function):
    db.configure_connection()
    try:
        db.execute_query("FIND QUERYTEMPLATE Test*").delete()
    except Exception as e:
        print(e)


def test_insertion_success():
    return db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="FIND RECORD Experiment WHICH HAS A animal=Pig").insert()


def test_insertion_failure_syntax():
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="SDASDUASIUF")
    with raises(db.TransactionError) as cm:
        q.insert()
    print(cm.value)
    assert (cm.value.errors[0].msg == "An error occured during the parsing of this query. "
            "Maybe you were using a wrong syntax?")


def test_insertion_failure_count_query_not_allowed():
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query="COUNT something")
    with raises(db.TransactionError) as cm:
        q.insert()
    assert (cm.value.errors[0].msg ==
            "QueryTemplates may not be defined by 'COUNT' queries for consistency reasons.")


def test_insertion_failure_select_query_not_allowed():
    query_def = "SELECT TestAnimal FROM TestExperiment WHICH HAS A TestAnimal = Pig"
    q = db.QueryTemplate(
        name="TestQueryTemplate",
        description="Find some interesting things with via a simple name.",
        query=query_def)
    with raises(db.TransactionError) as cm:
        q.insert()
    assert (cm.value.errors[0].msg ==
            "QueryTemplates may not be defined by 'SELECT ... FROM ...' queries for consistency reasons.")


def test_deletion_success():
    eid = test_insertion_success().id
    q = db.QueryTemplate(id=eid).delete()
    assert_true(q.is_deleted())


def test_deletion_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    with raises(db.TransactionError) as cm:
        q.delete()
    assert cm.value.has_error(db.EntityDoesNotExistError)


def test_retrieve_success():
    test_insertion_success()
    q = db.QueryTemplate(name="TestQueryTemplate").retrieve(sync=False)
    assert_true(q.is_valid())
    assert_is_not_none(q.query)
    assert_equal(q.query, "FIND RECORD Experiment WHICH HAS A animal=Pig")


def test_retrieve_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    with raises(db.TransactionError) as cm:
        q.retrieve()
    assert cm.value.has_error(db.EntityDoesNotExistError)


def test_update_success():
    q = test_insertion_success()
    q.query = "FIND NewStuff"
    q.update()


def test_update_failure_syntax():
    q = test_insertion_success()
    q.query = "ashdjfkasjdf"
    with raises(db.TransactionError) as cm:
        q.update()
    assert (cm.value.errors[0].msg == "An error occured during the parsing of this query. Maybe you "
            "were using a wrong syntax?")


def test_update_failure_count_query_not_allowed():
    q = test_insertion_success()
    q.query = "COUNT somethingNew"
    with raises(db.TransactionError) as cm:
        q.update()
    assert (cm.value.errors[0].msg ==
            "QueryTemplates may not be defined by 'COUNT' queries for consistency reasons.")


def test_update_failure_select_query_not_allowed():
    q = test_insertion_success()
    q.query = "SELECT TestAnimal FROM TestExperiment WHICH HAS A TestAnimal = Pig"
    with raises(db.TransactionError) as cm:
        q.update()
    assert (cm.value.errors[0].msg == "QueryTemplates may not be defined by 'SELECT ... FROM ...' "
            "queries for consistency reasons.")


def test_update_failure_non_existing():
    q = db.QueryTemplate(id="12342")
    q.query = "FIND NewStuff"
    with raises(db.TransactionError) as cm:
        q.update()
    assert cm.value.has_error(db.EntityDoesNotExistError)


def test_retrieve_as_entity_success():
    q = test_insertion_success()
    e = db.Entity(id=q.id).retrieve()
    assert_equal(e.id, q.id)


def test_query_simple_find():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    r = db.execute_query(query_def, unique=True)
    assert_equal(r.name, "TestRecord")
    assert_equal(len(r.get_properties()), 3)
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()
    assert_equal(
        db.execute_query(
            "FIND QUERYTEMPLATE TestPigExperiment",
            unique=True).name,
        "TestPigExperiment")
    assert_equal(
        db.execute_query(
            "FIND QUERYTEMPLATE",
            unique=True).name,
        "TestPigExperiment")
    assert_equal(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")


def test_query_with_select_in_outer_query():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    r = db.execute_query(query_def, unique=True)
    assert_equal(r.name, "TestRecord")
    assert_equal(len(r.get_properties()), 3)
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()
    assert_equal(
        db.execute_query(
            "FIND QUERYTEMPLATE TestPigExperiment",
            unique=True).name,
        "TestPigExperiment")
    assert_equal(
        db.execute_query(
            "FIND QUERYTEMPLATE",
            unique=True).name,
        "TestPigExperiment")
    assert_equal(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")
    r = db.execute_query(
        "SELECT TestAnimal FROM TestPigExperiment",
        unique=True)
    assert_equal(r.name, "TestRecord")
    assert_equal(len(r.get_properties()), 1)


def test_query_with_other_filters():
    query_def = "FIND ENTITY TestExperiment WHICH HAS A TestAnimal"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("FIND ENTITY TestAnimalExperiment")
    assert_equal(len(r), 2)

    r = db.execute_query(
        "FIND TestAnimalExperiment WHICH HAS A TestAnimal=Pig",
        unique=True)
    assert_equal(r.name, "TestRecord")


def test_query_simple_find_with_wildcard():
    query_def = "FIND ENTITY TestExperiment WHICH HAS A TestAnimal = Pig"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("FIND ENTITY TestAnimal*")
    assert_equal(len(r), 2)

    r = db.execute_query(
        "FIND TestAnimal* WHICH HAS A TestAnimal=Pig",
        unique=True)
    assert_equal(r.name, "TestRecord")


def test_query_select_from_with_wildcard():
    query_def = "FIND ENTITY TestExperiment WHICH HAS A TestAnimal = Pig"
    db.QueryTemplate(name="TestAnimalExperiment", query=query_def).insert()

    r = db.execute_query("SELECT TestAnimal FROM ENTITY TestAnimal*")
    assert_equal(len(r), 2)
    assert_equal(len(r.get_entity_by_name("TestRecord").get_properties()), 1)
    assert_equal(len(r.get_entity_by_name("TestAnimal").get_properties()), 0)


def test_query_without_permission():
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal = Pig"
    qt = db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()

    db.administration._insert_user(
        name="test_user", password="secret_1q!Q", status="ACTIVE", email=None, entity=None)

    db.configure_connection(username="test_user", password="secret_1q!Q",
                            password_method="plain")

    r = db.execute_query(query_def, unique=True)
    assert_equal(r.name, "TestRecord")
    r = db.execute_query("FIND TestPigExperiment", unique=True)
    assert_equal(r.name, "TestRecord")

    db.configure_connection()

    e = db.Entity(id=qt.id)
    e.retrieve_acl()
    assert_is_not_none(e.acl)
    e.deny(username="test_user", permission="RETRIEVE:ENTITY")
    e.update_acl()

    db.configure_connection(username="test_user", password="secret_1q!Q",
                            password_method="plain")

    r = db.execute_query(query_def, unique=True)
    assert_equal(r.name, "TestRecord")
    r = db.execute_query("FIND TestPigExperiment")
    assert_equal(len(r), 0)


def test_query_with_subquery_referenced_by():
    assert_equal(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY A TestExperiment AS A TestConductor",
            unique=True).name,
        "TestJohnDoe")
    query_def = "FIND TestExperiment WHICH HAS A TestAnimal=Pig"
    assert_equal(db.execute_query(query_def, unique=True).name, "TestRecord")
    db.QueryTemplate(name="TestPigExperiment", query=query_def).insert()

    assert_equal(
        db.execute_query(
            "FIND TestPigExperiment",
            unique=True).name,
        "TestRecord")

    assert_equal(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestPigExperiment AS A TestConductor",
            unique=True).name,
        "TestJohnDoe")
    assert_equal(
        db.execute_query(
            "FIND TestPerson WHICH IS REFERENCED BY TestPigExperiment",
            unique=True).name,
        "TestJohnDoe")
