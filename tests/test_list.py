# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 19.04.2017.

@author: tf
"""
import os
import linkahead as db
from linkahead.exceptions import (TransactionError,
                                  UnqualifiedPropertiesError)
from pytest import raises


def assert_equal(a, b):
    assert a == b


def assert_true(a):
    assert a


def setup_function(function):
    d = db.execute_query("FIND ENTITY WITH ID > 99")
    if len(d) > 0:
        d.delete()


def teardown_function(function):
    if os.path.isfile("test.dat"):
        os.remove("test.dat")
    setup_function(function)


def test_list_of_files():
    p = db.Property(name="TestListProperty", datatype=db.FILE).insert()
    upload_file = open("test.dat", "w")
    upload_file.write("hello world\n")
    upload_file.close()
    file_ = db.File(name="Testfile1",
                    description="Testfile Desc",
                    path="testfiles/testfile.dat",
                    file="test.dat")
    file_.insert()

    recty = db.RecordType(name="TestRT").insert()
    rec = db.Record(
        name="TestRecOk").add_parent(recty).add_property(
            name="TestListProperty",
            datatype=db.LIST(
                db.FILE),
            value=[
                file_.id]).insert()
    assert rec.is_valid()

    with raises(TransactionError) as tr_err:
        db.Record(
            name="TestRecNotOk").add_parent(recty).add_property(
                name="TestListProperty",
                datatype=db.LIST(
                    db.FILE),
                value=[
                    p.id]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Reference not qualified. The value of this Reference Property is to be a child of its data type.")


def test_list_datatype_know():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    assert_equal(
        p.id,
        db.execute_query(
            "FIND ENTITY TestListProperty",
            unique=True).id)
    assert_equal(
        p.datatype,
        db.execute_query(
            "FIND ENTITY TestListProperty",
            unique=True).datatype)


def test_rt_property_without_value():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    rt = db.RecordType(
        name="TestRT").add_property(
            name="TestListProperty").insert()
    assert_true(rt.is_valid())

    assert_equal(
        p.id,
        db.execute_query(
            "FIND ENTITY TestRT",
            unique=True).get_property("TestListProperty").id)
    assert_equal(
        p.datatype,
        db.execute_query(
            "FIND ENTITY TestRT",
            unique=True).get_property("TestListProperty").datatype)


def test_concrete_property_with_single_value():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    rt = db.RecordType(
        name="TestRT").add_property(
            name="TestListProperty",
            value=[1]).insert(
                sync=False)
    assert_equal("[1]", str(rt.get_property("TestListProperty").value))
    assert_equal([1], rt.get_property("TestListProperty").value)
    assert_true(rt.is_valid())

    assert_equal(
        p.id,
        db.execute_query(
            "FIND ENTITY TestRT",
            unique=True).get_property("TestListProperty").id)
    assert_equal(
        p.datatype,
        db.execute_query(
            "FIND ENTITY TestRT",
            unique=True).get_property("TestListProperty").datatype)
    assert_equal(
        "[1]", str(
            db.execute_query(
                "FIND ENTITY TestRT", unique=True).get_property("TestListProperty").value))
    assert_equal(
        [1],
        db.execute_query(
            "FIND ENTITY TestRT",
            unique=True).get_property("TestListProperty").value)


def test_query_concrete_property_with_single_value():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    rt = db.RecordType(
        name="TestRT").add_property(
            name="TestListProperty",
            value=[1337]).insert()
    assert_true(rt.is_valid())

    assert_equal(
        rt.id,
        db.execute_query(
            "FIND ENTITY TestRT WHICH HAS A TestListProperty=1337",
            unique=True).id)


def test_query_concrete_property_with_more_values():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    rt = db.RecordType(
        name="TestRT").add_property(
            name="TestListProperty", value=[
                1337, 2, 3]).insert()
    assert_true(rt.is_valid())

    assert_equal(
        rt.id,
        db.execute_query(
            "FIND ENTITY TestRT WHICH HAS A TestListProperty=1337",
            unique=True).id)


def test_error_on_wrong_value():
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            db.INTEGER)).insert()
    assert_true(p.is_valid())

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT").add_property(
                name="TestListProperty",
                value=["this is not an int"]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Cannot parse value to integer.")


def test_data_type_with_non_existing_ref1():
    with raises(TransactionError) as tr_err:
        db.Property(name="TestListProperty",
                    datatype=db.LIST("non_existing")).insert()
    assert tr_err.value.errors[0].msg == "Unknown data type."


def test_data_type_with_non_existing_ref2():
    with raises(TransactionError) as tr_err:
        db.Property(
            name="TestListProperty",
            datatype=db.LIST(234233234)).insert()
    assert tr_err.value.errors[0].msg == "Unknown data type."


def test_data_type_with_non_existing_ref3():
    with raises(TransactionError) as tr_err:
        db.Property(name="TestListProperty", datatype=db.LIST(-2341)).insert()
    assert tr_err.value.errors[0].msg == "Unknown data type."


def test_data_type_with_existing_ref1():
    c = db.Container().append(db.RecordType(name="TestRT")).append(
        db.Property(name="TestListProperty", datatype=db.LIST("TestRT")))
    c.insert()
    assert_true(c.is_valid())


def test_data_type_with_existing_ref2():
    rt = db.RecordType(name="TestRT")
    c = db.Container().append(rt).append(
        db.Property(name="TestListProperty", datatype=db.LIST(rt)))
    c.insert()
    assert_true(c.is_valid())


def test_data_type_with_existing_ref3():
    rt = db.RecordType(name="TestRT").insert()
    p = db.Property(name="TestListProperty", datatype=db.LIST(rt)).insert()
    assert_true(p.is_valid())


def test_data_type_with_existing_ref4():
    rt = db.RecordType(name="TestRT").insert()
    p = db.Property(name="TestListProperty", datatype=db.LIST(rt.id)).insert()
    assert_true(p.is_valid())


def test_data_type_with_existing_ref5():
    rt = db.RecordType(name="TestRT").insert()
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            rt.name)).insert()
    assert_true(p.is_valid())


def test_rt_ref_property_without_value():
    rt = db.RecordType(name="TestRT").insert()
    p = db.Property(
        name="TestListProperty",
        datatype=db.LIST(
            rt.name)).insert()
    assert_true(p.is_valid())

    rt2 = db.RecordType(
        name="TestRT2").add_property(
            name="TestListProperty").insert()
    assert_true(rt2.is_valid())
    assert_equal(
        rt2.get_property("TestListProperty").datatype,
        db.LIST(
            rt.name))

    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2",
            unique=True).get_property("TestListProperty").datatype,
        db.LIST(
            rt.name))


def test_datatype_inheritance1():
    rt = db.RecordType(name="TestRT").insert()
    db.Property(name="TestListProperty", datatype=db.LIST(rt.name)).insert()
    p2 = db.Property(
        name="TestListProperty2").add_parent(
            name="TestListProperty").insert()

    assert_equal(p2.datatype, db.LIST(rt.name))
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestListProperty2",
            unique=True).datatype,
        db.LIST(
            rt.name))


def test_datatype_inheritance2():
    rt = db.RecordType(name="TestRT").insert()
    rt2 = db.RecordType(name="TestRT2").add_parent(name="TestRT").insert()

    db.Property(name="TestListProperty", datatype=db.LIST(rt.name)).insert()
    p2 = db.Property(
        name="TestListProperty2",
        datatype=db.LIST(
            rt2.name)).add_parent(
                name="TestListProperty").insert()

    assert_equal(p2.datatype, db.LIST(rt2.name))
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestListProperty2",
            unique=True).datatype,
        db.LIST(
            rt2.name))


def test_datatype_inheritance3():
    rt = db.RecordType(name="TestRT").insert()
    rt2 = db.RecordType(name="TestRT2").insert()

    db.Property(name="TestListProperty", datatype=db.LIST(rt.name)).insert()
    p2 = db.Property(
        name="TestListProperty2",
        datatype=db.LIST(
            rt2.name)).add_parent(
                name="TestListProperty").insert()

    assert_equal(p2.datatype, db.LIST(rt2.name))
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestListProperty2",
            unique=True).datatype,
        db.LIST(
            rt2.name))


def test_single_ref_value():
    rt = db.RecordType(name="TestRT").insert()
    rec = db.Record(name="TestRec").add_parent(name="TestRT").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()

    rt2 = db.RecordType(
        name="TestRT2").add_property(
            name="TestProp",
            value=[rec]).insert()
    assert_true(rt2.is_valid())


def test_single_ref_value_scope_error():
    db.RecordType(name="TestRT").insert()
    rec = db.Record(name="TestRec").add_parent(name="TestRT").insert()

    rt2 = db.RecordType(name="TestRT2").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt2)).insert()

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT3").add_property(
                name="TestProp",
                value=[rec]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Reference not qualified. The value of this Reference " +
            "Property is to be a child of its data type.")


def test_error_single_non_existing_ref_value():
    rt = db.RecordType(name="TestRT").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT2").add_property(
                name="TestProp",
                value=["non_existing"]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT2").add_property(
                name="TestProp",
                value=[213425234234]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")

    with raises(TransactionError) as tr_err:
        db.RecordType(name="TestRT2").add_property(
            name="TestProp", value=[-1234]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")


def test_error_multi_non_existing_ref_value():
    rt = db.RecordType(name="TestRT").insert()
    db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    db.Record(name="TestRec2").add_parent(name="TestRT").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT2").add_property(
                name="TestProp",
                value=[
                    "TestRec1",
                    "non_existing",
                    "TestRec2"]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT2").add_property(
            name="TestProp",
                value=[
                    213425234234,
                    "TestRec2",
                    "TestRec1"]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT2").add_property(
                name="TestProp", value=[
                    "TestRec1", "TestRec2", -1234]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")


def test_multi_ref_value_scope_error():
    db.RecordType(name="TestRT").insert()
    rec = db.Record(name="TestRec").add_parent(name="TestRT").insert()

    rt2 = db.RecordType(name="TestRT2").insert()
    db.Record(name="TestRec1").add_parent(name="TestRT2").insert()
    db.Record(name="TestRec2").add_parent(name="TestRT2").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt2)).insert()

    with raises(TransactionError) as tr_err:
        db.RecordType(
            name="TestRT3").add_property(
                name="TestProp",
                value=[
                    rec,
                    "TestRec1",
                    "TestRec2"]).insert()
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            "Reference not qualified. The value of this Reference " +
            "Property is to be a child of its data type.")


def test_multi_ref_value():
    rt = db.RecordType(name="TestRT").insert()
    rec = db.Record(name="TestRec").add_parent(name="TestRT").insert()
    rec2 = db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    db.Record(name="TestRec2").add_parent(name="TestRT").insert()
    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()

    rt2 = db.RecordType(
        name="TestRT2").add_property(
            name="TestProp", value=[
                rec, rec2.id, "TestRec2"]).insert()
    assert_true(rt2.is_valid())

    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec2",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec1",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=" + str(rec.id),
            unique=True).id, rt2.id)


def test_multi_ref_with_doublets():
    rt = db.RecordType(name="TestRT").insert()
    rec1 = db.Record(name="TestRec").add_parent(name="TestRT").insert()
    rec2 = db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    rec3 = db.Record(name="TestRec2").add_parent(name="TestRT").insert()

    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()
    rt2 = db.RecordType(
        name="TestRT2").add_property(
            name="TestProp",
            value=[
                rec1,
                rec2,
                rec3,
                rec2,
                rec1]).insert()
    assert_true(rt2.is_valid())
    assert_equal(len(rt2.get_property("TestProp").value), 5)

    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec2",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec1",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=" + str(rec1.id),
            unique=True).id, rt2.id)


def test_multi_ref_with_null():
    rt = db.RecordType(name="TestRT").insert()
    rec1 = db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    rec2 = db.Record(name="TestRec2").add_parent(name="TestRT").insert()

    db.Property(name="TestProp", datatype=db.LIST(rt)).insert()
    rt2 = db.RecordType(
        name="TestRT2").add_property(
            name="TestProp",
            value=[
                rec1,
                rec2,
                None]).insert()
    assert_true(rt2.is_valid())
    assert_equal(len(rt2.get_property("TestProp").value), 3)
    assert_equal(rt2.get_property("TestProp").value[0], rec1.id)
    assert_equal(rt2.get_property("TestProp").value[1], rec2.id)
    assert_equal(rt2.get_property("TestProp").value[2], None)

    assert_equal(db.execute_query("FIND ENTITY TestRT2", unique=True).id, rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec2",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WHICH HAS A TestProp=TestRec1",
            unique=True).id,
        rt2.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRT2 WITH TestProp IS NULL",
            unique=True).id,
        rt2.id)


def test_rec_with_value():
    rt = db.RecordType(name="TestRT").insert()
    rec1 = db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    rec2 = db.Record(name="TestRec2").add_parent(name="TestRT").insert()
    rec3 = db.Record(name="TestRec3").add_parent(name="TestRT").insert()

    rt = db.RecordType(name="TestRT2", datatype=db.LIST(rt)).insert()
    rec4 = db.Record(name="TestRec4").add_parent(name="TestRT2")
    rec4.value = [rec1, rec2, rec3]
    rec4.insert()

    assert_true(rec4.is_valid())
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRec4 WHICH REFERENCES TestRec3",
            unique=True).id,
        rec4.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRec4 WHICH REFERENCES TestRec2",
            unique=True).id,
        rec4.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRec4 WHICH REFERENCES TestRec1",
            unique=True).id,
        rec4.id)


def test_rec_with_null_value():
    rt = db.RecordType(name="TestRT").insert()
    rec1 = db.Record(name="TestRec1").add_parent(name="TestRT").insert()
    rec2 = db.Record(name="TestRec2").add_parent(name="TestRT").insert()

    rt = db.RecordType(name="TestRT2", datatype=db.LIST(rt)).insert()
    rec4 = db.Record(name="TestRec4").add_parent(name="TestRT2")
    rec4.value = [rec1, rec2, None]
    rec4.insert()

    assert_true(rec4.is_valid())
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRec4 WHICH REFERENCES TestRec1",
            unique=True).id,
        rec4.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestRec4 WHICH REFERENCES TestRec2",
            unique=True).id,
        rec4.id)


def test_list_of_references():
    rt1 = db.RecordType(name="Test_RT1").insert()
    rt2 = db.RecordType(
        name="Test_RT2").add_property(
            name="Test_RT1",
            datatype=db.LIST("Test_RT1")).insert()
    p = rt2.get_properties()[0]
    assert p.id == rt1.id
    assert p.name == rt1.name
    assert p.datatype == db.LIST(rt1.name)

    p = db.execute_query("FIND ENTITY Test_RT2", unique=True).get_properties()[0]
    assert p.id == rt1.id
    assert p.name == rt1.name
    assert p.datatype == db.LIST(rt1.name)

    rt1rec1 = db.Record(name="Test_RT1_Rec1").add_parent("Test_RT1").insert()
    assert rt1rec1.is_valid()

    with raises(TransactionError) as tr_err:
        db.Record(
            name="Test_RT2_Rec").add_parent("Test_RT2").add_property(
                name="Test_RT1",
                value=[rt1rec1]).insert()
    print(tr_err.value)
    assert tr_err.value.has_error(UnqualifiedPropertiesError)
    assert (tr_err.value.errors[0].errors[0].msg ==
            'This data type does not accept collections of values (e.g. Lists).')
    rt2rec = db.Record(name="Test_RT2_Rec").add_parent("Test_RT2").add_property(
        name="Test_RT1", datatype=db.LIST("Test_RT1"), value=[rt1rec1]).insert()
    assert rt2rec.is_valid()


def test_list_in_sub_property():
    db.RecordType(name="TestRT1").insert()
    db.RecordType(name="TestRT2").insert()
    db.Property(name="TestProperty", datatype="TestRT1").insert()
    db.Property(name="TestBogusProperty", datatype=db.TEXT).insert()
    db.Record(name="TestRT1Rec1").add_parent("TestRT1").insert()
    rec2 = db.Record(name="TestRT2Rec2").add_parent("TestRT2").add_property(
        "TestProperty", "TestRT1Rec1")
    rec2.get_property("TestProperty").add_property("TestBogusProperty",
                                                   datatype=db.LIST(db.TEXT))
    rec2.insert()

    rec2.delete()
