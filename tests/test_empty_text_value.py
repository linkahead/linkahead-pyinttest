#!/usr/bin/python2
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import pytest

import caosdb as db


def setup_function(function):
    teardown_function(function)
    rt = db.RecordType("TestRT")
    rt.insert()
    p = db.Property("TestProp", datatype=db.TEXT)
    p.insert()


def teardown_function(function):
    try:
        db.execute_query("FIND ENTITY").delete()
    except Exception as e:
        print(e)


def test_empty_string():
    r1 = db.Record()
    r1.add_parent("TestRT")
    r1.add_property("TestProp", value="")
    r1.insert()

    # value was stored correctly
    assert db.execute_query("FIND Record TestRT", unique=True).get_property(
        "TestProp").value == ""
    # query language works for empty string
    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp=''", unique=True).id == r1.id
    assert db.execute_query('FIND ENTITY TestRT with TestProp=""',
                            unique=True).get_property("TestProp").value == ""

    r2 = db.Record()
    r2.add_parent("TestRT")
    r2.add_property("TestProp", value="not empty")
    r2.insert()

    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp='not empty'", unique=True).id == r2.id
    # query language work while other records with non empty values are present
    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp=''", unique=True).id == r1.id
    assert len(db.execute_query("FIND Record TestRT")) == 2


def test_null_value():
    r1 = db.Record()
    r1.add_parent("TestRT")
    r1.add_property("TestProp", value=None)
    r1.insert()

    # value was stored correctly
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value is None
    # query language works with null value
    assert db.execute_query(
        "FIND ENTITY TestRT WHERE TestProp IS NULL", unique=True).id == r1.id

    # add a bit of noise
    r2 = db.Record()
    r2.add_parent("TestRT")
    r2.add_property("TestProp", value="null")
    r2.insert()

    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp='null'", unique=True).id == r2.id
    # query language works while other record with non-null values are present
    assert db.execute_query(
        "FIND ENTITY TestRT WHERE TestProp IS NULL", unique=True).id == r1.id
    assert len(db.execute_query("FIND Record TestRT")) == 2


def test_list_with_empty_string():
    r1 = db.Record()
    r1.add_parent("TestRT")
    r1.add_property("TestProp", datatype=db.LIST(db.TEXT), value=[""])
    r1.insert()

    # value was stored correctly
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value == [""]
    # query language works
    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp=''", unique=True).id == r1.id

    r2 = db.Record()
    r2.add_parent("TestRT")
    r2.add_property("TestProp", datatype=db.LIST(db.TEXT), value=["leer"])
    r2.insert()
    assert db.execute_query("FIND Record TestRT with TestProp='leer'",
                            unique=True).get_property("TestProp").value == ["leer"]
    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp='leer'", unique=True).id == r2.id
    assert db.execute_query(
        "FIND ENTITY TestRT with TestProp=''", unique=True).id == r1.id


def test_null_list():
    r1 = db.Record()
    r1.add_parent("TestRT")
    r1.add_property("TestProp", datatype=db.LIST(db.TEXT), value=None)
    r1.insert()

    # null list was stored correctly
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value is None
    assert db.execute_query(
        "FIND ENTITY TestRT WHERE TestProp IS NULL", unique=True).id == r1.id


@pytest.mark.xfail(reason="""this is the confirmation for
                  https://gitlab.com/caosdb/caosdb-server/issues/new. Empty
                  lists cannot be distinguished from None.""")
def test_empty_list():
    r = db.Record()
    r.add_parent("TestRT")
    r.add_property("TestProp", datatype=db.LIST(db.TEXT), value=[])
    r.insert()
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value == []


@pytest.mark.xfail(reason="""this is the confirmation for
                  https://gitlab.com/caosdb/caosdb-server/issues/new. `None`
                  cannot be distinguished from [None] lists.""")
def test_list_with_null_value():
    r = db.Record()
    r.add_parent("TestRT")
    r.add_property("TestProp", datatype=db.LIST(db.TEXT), value=["null"])
    r.insert()
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value == ["null"]
    r.delete()
    r = db.Record()
    r.add_parent("TestRT")
    r.add_property("TestProp", datatype=db.LIST(db.TEXT), value=[None])
    r.insert()
    assert db.execute_query("FIND Record TestRT",
                            unique=True).get_property("TestProp").value == [None]
    # assert db.execute_query("FIND TestRT WHERE TestProp IS NULL", unique=True).id == r.id
    r.delete()
