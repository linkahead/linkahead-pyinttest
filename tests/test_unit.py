# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 14.12.2015.

@author: tf
"""
from __future__ import unicode_literals
import caosdb as h


def setup_module():
    try:
        h.execute_query("FIND ENTITY").delete()
    except BaseException:
        pass


def teardown_module():
    setup_module()


def teardown_function(function):
    setup_module()


def test_km_1():
    # insert with base unit and search for base unit and derived unit

    p = h.Property(
        name="SimpleDoubleProperty",
        datatype=h.DOUBLE,
        unit="m")
    p.insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p, value=3140)
    rt.insert()

    rt2 = h.execute_query("FIND ENTITY SimpleRecordType", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id
    assert rt2.get_property("SimpleDoubleProperty").unit == "m"
    assert rt2.get_property("SimpleDoubleProperty").value == 3140.0

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3140.0m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3140m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3.14km'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_km_2():
    # insert with derived unit and search for base unit and derived unit

    p = h.Property(
        name="SimpleDoubleProperty",
        datatype="DOUBLE",
        unit="km")
    p.insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p, value=3.14)
    rt.insert()

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3.14km'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3140.0m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='3140m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_celsius_1():
    # insert with base unit and search for base unit and derived unit

    p = h.Property(
        name="SimpleDoubleProperty",
        datatype=h.DOUBLE,
        unit="K")
    p.insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p, value=0)
    rt.insert()

    rt2 = h.execute_query("FIND ENTITY SimpleRecordType", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id
    assert rt2.get_property("SimpleDoubleProperty").unit == "K"
    assert rt2.get_property("SimpleDoubleProperty").value == 0.0

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='0.0K'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='0K'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='-273.15°C'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_celsius_2():
    # insert with derived unit and search for base unit and derived unit

    p = h.Property(
        name="SimpleDoubleProperty",
        datatype="DOUBLE",
        unit="°C")
    p.insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p, value=0)
    rt.insert()

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='0.0°C'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='0°C'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleDoubleProperty='273.15K'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_int():
    # insert with base unit and search for base unit and derived unit

    p = h.Property(
        name="SimpleIntegerProperty",
        datatype=h.INTEGER,
        unit="m")
    p.insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p, value=3140)
    rt.insert()

    rt2 = h.execute_query("FIND ENTITY SimpleRecordType", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id
    assert rt2.get_property("SimpleIntegerProperty").unit == "m"
    assert rt2.get_property("SimpleIntegerProperty").value == 3140

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3140m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3140.0m'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3.14km'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='314000cm'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='314000.0cm'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_unknown_unit():
    # insert unknown unit

    p = h.Property(
        name="SimpleIntegerProperty",
        datatype=h.INTEGER,
        unit="my_unit")
    p.insert()
    assert p.is_valid()
    assert len(p.get_warnings()) == 1
    assert p.get_warnings()[
        0].description == "Unknown unit. Values with this unit cannot be converted to other units when used in search queries."

    rt = h.RecordType(
        name="SimpleRecordType").add_property(
        p, value="3140")
    rt.insert()

    rt2 = h.execute_query("FIND ENTITY SimpleRecordType", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id
    assert rt2.get_property("SimpleIntegerProperty").unit == "my_unit"
    assert rt2.get_property("SimpleIntegerProperty").value == 3140

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3140'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3140my_unit'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id

    rt2 = h.execute_query(
        "FIND ENTITY SimpleRecordType.SimpleIntegerProperty='3140.0my_unit'", True)
    assert rt2.is_valid()
    assert rt2.id, rt.id


def test_greatest():
    # insert unknown unit

    p = h.Property(
        name="SimpleDoubleProperty",
        datatype=h.DOUBLE,
        unit="m")
    p.insert()

    rt = h.RecordType(name="SimpleRecordType").add_property(p)
    rt.insert()

    rec1 = h.Record(name="SimpleRecord1").add_parent(
        rt).add_property(p, unit="km", value="1").insert()
    rec2 = h.Record(name="SimpleRecord2").add_parent(
        rt).add_property(p, unit="km", value="2").insert()
    rec3 = h.Record(
        name="SimpleRecord3").add_parent(rt).add_property(
        p, unit="m", value="200").insert()

    assert rec1.is_valid()
    assert rec2.is_valid()
    assert rec3.is_valid()

    c = h.execute_query(
        "FIND ENTITY SimpleRecord* WHICH HAS THE GREATEST SimpleDoubleProperty",
        unique=True)
    assert c.id == rec2.id

    c = h.execute_query(
        "FIND ENTITY SimpleRecord* WHICH HAS THE SMALLEST SimpleDoubleProperty",
        unique=True)
    assert c.id == rec3.id

    rec4 = h.Record(
        name="SimpleRecord4").add_parent(rt).add_property(
        p, unit="bla", value="150").insert()
    assert rec4.is_valid()

    c = h.execute_query(
        "FIND ENTITY SimpleRecord* WHICH HAS THE GREATEST SimpleDoubleProperty")
    assert c[0].id == rec3.id
    assert c.get_warnings()[0].description, "The filter POV(SimpleDoubleProperty,NULL == NULL) with the aggregate function 'max' could not match the values against each other with their units. The values had different base units. Only their numric value had been taken into account."

    c = h.execute_query(
        "FIND ENTITY SimpleRecord* WHICH HAS THE SMALLEST SimpleDoubleProperty",
        unique=True)
    assert c.id == rec1.id
    assert c.get_warnings()[0].description, "The filter POV(SimpleDoubleProperty,NULL == NULL) with the aggregate function 'min' could not match the values against each other with their units. The values had different base units. Only their numric value had been taken into account."
