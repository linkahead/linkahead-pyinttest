# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 21.03.2017.

@author: tf
"""

import linkahead as db
from pytest import raises, mark


def assert_true(a):
    assert a


def assert_equal(a, b):
    assert a == b


def setup_module():
    pass


def teardown_module():
    teardown_function(None)


def setup_function(function):
    try:
        db.execute_query("FIND ENTITY Test*").delete()
    except BaseException:
        pass


def teardown_function(function):
    try:
        db.execute_query("FIND ENTITY Test*").delete()
    except BaseException:
        pass


def test_property_insertion():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name").insert()
    assert p.is_valid()

    return p


def test_property_query():
    pid = test_property_insertion().id
    assert_equal(db.execute_query("FIND ENTITY TestShortName", unique=True).id, pid)

    # the short name does not apply to the property itself!!!
    assert_equal(len(db.execute_query("FIND ENTITY TestSN")), 0)


def test_recordtype_insertion_separately_prop_by_id_direct_name_child():
    pid = test_property_insertion().id
    rt = db.RecordType(
        name="TestRecordType").add_property(
        id=pid, value="TestRT").insert()
    assert rt.is_valid()

    return rt


def test_recordtype_is_stored_correctly():
    test_recordtype_insertion_separately_prop_by_id_direct_name_child()

    rt = db.RecordType(name="TestRecordType").retrieve()
    assert rt.is_valid()
    assert_equal(len(rt.get_properties()), 1)
    assert rt.get_property("TestShortName") is not None
    assert_equal(rt.get_property("TestShortName").value, "TestRT")


def test_recordtype_insertion_container_prop_by_tempid_direct_name_child():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name")
    p.id = -1
    rt = db.RecordType(
        name="TestRecordType").add_property(
        id=-1, value="TestRT")
    c = db.Container().extend([p, rt])
    c.insert()
    assert c.is_valid()
    assert p.is_valid()
    assert rt.is_valid()
    return rt


def test_recordtype_insertion_container_prop_by_name_direct_name_child():
    p = db.Property(
        name="TestShortName",
        datatype=db.TEXT,
        value="TestSN").add_parent(
        name="name")
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestShortName",
        value="TestRT")
    c = db.Container().extend([p, rt])
    c.insert()
    assert c.is_valid()
    assert p.is_valid()
    assert rt.is_valid()
    return rt


def test_recordtype_insertion_separately_prop_by_name_direct_name_child():
    test_property_insertion()
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestShortName",
        value="TestRT").insert()
    assert rt.is_valid()
    return rt


def test_recordtpye_insertion_with_indirect_child_with_existing_parent():
    test_property_insertion()
    p = db.Property(
        name="TestExtraShortName").add_parent(
        name="TestShortName").insert()
    assert p.is_valid()

    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestExtraShortName",
        value="TestRT").insert()
    assert rt.is_valid()
    return rt


def test_recordtpye_insertion_with_indirect_child_with_new_parent():
    parp = db.Property(name="TestShortName").add_parent(name="name")
    p = db.Property(name="TestExtraShortName").add_parent(name="TestShortName")
    rt = db.RecordType(
        name="TestRecordType").add_property(
        name="TestExtraShortName",
        value="TestRT")
    c = db.Container().extend([parp, p, rt]).insert()

    assert c.is_valid()
    assert p.is_valid()
    assert parp.is_valid()
    assert rt.is_valid()

    return rt


def assert_same_unique_results(call, queries):
    setup_function(None)
    rtid = call().id
    for q in queries:
        assert_equal(db.execute_query(q, unique=True).id, rtid)
    teardown_function(None)


def test_recordtype_query():
    assert_same_unique_results(
        test_recordtype_insertion_separately_prop_by_id_direct_name_child, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])
    assert_same_unique_results(
        test_recordtype_insertion_separately_prop_by_name_direct_name_child, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])
    assert_same_unique_results(
        test_recordtype_insertion_container_prop_by_name_direct_name_child, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])
    assert_same_unique_results(
        test_recordtype_insertion_container_prop_by_tempid_direct_name_child, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])
    assert_same_unique_results(
        test_recordtpye_insertion_with_indirect_child_with_new_parent, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])
    assert_same_unique_results(
        test_recordtpye_insertion_with_indirect_child_with_existing_parent, [
            "FIND ENTITY TestRecordType", "FIND ENTITY TestRT"])


def test_query_name_property():
    """ Insert a Property which has a name parent. Add to Record and query the
    record."""
    # test behavior without the name parent
    db.RecordType("TestPerson").insert()
    db.Property("TestGivenName", datatype=db.TEXT).insert()
    rec = db.Record("TestJohnDoe").add_parent("TestPerson")
    rec.add_property("TestGivenName", "John")
    rec.insert()

    assert db.execute_query("FIND ENTITY TestPerson WITH TestGivenName='John'",
                            unique=True).id == rec.id
    with raises(db.BadQueryError):
        db.execute_query("FIND John", unique=True)

    teardown_function(None)

    # test behavior WITH the name parent
    db.RecordType("TestPerson").insert()
    db.Property("TestGivenName").add_parent("name").insert()
    rec = db.Record("TestJohnDoe").add_parent("TestPerson")
    rec.add_property("TestGivenName", "John")
    rec.insert()

    assert db.execute_query("FIND ENTITY TestPerson WITH TestGivenName='John'",
                            unique=True).id == rec.id
    assert db.execute_query("FIND John",
                            unique=True).id == rec.id


def test_query_property_with_pov():
    """ Insert a Record with a property which can be searched using two
    different names.

    The TestRating is the primary name of the property while TestRating is a
    Synonym.
    """
    db.RecordType(name="TestExperiment").insert()
    db.Property(
        name="TestSynonym",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.Property(
        name="TestRating",
        datatype=db.INTEGER).add_property(
        name="TestSynonym",
        value="TestScore").insert()
    rec = db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestRating",
        value=4).insert()

    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestRating=4",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestRating>3",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestScore=4",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestScore>3",
            unique=True).id,
        rec.id)


def test_query_with_reference():
    """ Insert a Record with two names. Both work in a reference query.

    The record has a primary name ("TestJohnDoe") and a property
    `TestGivenName` (="John") with also works with the query.
    """
    db.RecordType(name="TestExperiment").insert()
    db.RecordType(name="TestPerson").insert()
    db.Property(
        name="TestGivenName",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    db.Record(
        name="TestJohnDoe").add_parent(
        name="TestPerson").add_property(
            name="TestGivenName",
        value="John").insert()
    rec = db.Record(
        name="TestRecord").add_parent(
        name="TestExperiment").add_property(
            name="TestConductor",
        value="TestJohnDoe").insert()
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestConductor=TestJohnDoe",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestExperiment WHICH HAS A TestConductor=John",
            unique=True).id,
        rec.id)


def test_query_with_back_reference():
    """ Insert a Record with two names. Both work in a back-ref query.

    The record has a primary name ("TestMeasurement") and a property
    `TestSynomym` (="TestObservation") with also works with the query.
    """
    db.RecordType(name="TestPerson").insert()
    db.Property(
        name="TestSynonym",
        datatype=db.TEXT).add_parent(
        name="name").insert()
    db.RecordType(
        name="TestMeasurement").add_property(
        name="TestSynonym",
        value="TestObservation").insert()
    db.Property(name="TestConductor", datatype="TestPerson").insert()
    rec = db.Record(name="TestJohnDoe").add_parent(name="TestPerson").insert()
    db.Record(
        name="TestRecord").add_parent(
        name="TestMeasurement").add_property(
            name="TestConductor",
        value="TestJohnDoe").insert()
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestPerson WHICH IS REFERENCED BY TestMeasurement",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestPerson WHICH IS REFERENCED BY TestMeasurement AS A TestConductor",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestPerson WHICH IS REFERENCED BY TestObservation",
            unique=True).id,
        rec.id)
    assert_equal(
        db.execute_query(
            "FIND ENTITY TestPerson WHICH IS REFERENCED BY TestObservation AS A TestConductor",
            unique=True).id,
        rec.id)


@mark.xfail(reason="Enhancement proposed")
def test_name_resource():
    name_prop = db.Property(name="name").retrieve()
    alias_prop = db.Property(name="TestAlias").add_parent(name_prop).insert()

    rt = db.RecordType(
        name="TestRT_primary").add_property(
        alias_prop,
        value="TestRT_alias").insert()

    connection = db.get_connection()

    flags = {"names": None}

    http_response = connection.retrieve(
        entity_uri_segments=["Entity"],
        query_dict=flags)
    result = db.Container._response_to_entities(http_response)
    print(result)
    names = [e.name for e in result]
    assert "TestRT_primary" in names
    assert "TestRT_alias" in names  # the alias should also be included"
